window.addEventListener("DOMContentLoaded", function () {
    const S = 100;
    const M = 200;
    const L = 300;
    const EXTRA_PRICE = 0;
    const PUR = 10;
    const RED = 20;
    const BLUE = 30;
    const CHB = 40;
    let price = S;
    let extraPrice = EXTRA_PRICE;
    let result;
    let calc = document.getElementById("calc");
    let myNum = document.getElementById("number");
    let resultSpan = document.getElementById("result-span");
    let select = document.getElementById("select");
    let radiosDiv = document.getElementById("radio-div");
    let radios = document.querySelectorAll("#radio-div input[type=radio]");
    let checkboxDiv = document.getElementById("check-div");
    let checkbox = document.getElementById("checkbox");
    select.addEventListener("change", function (event) {
        let option = event.target;
        if (option.value === "1") {
            radiosDiv.classList.add("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = S;
        }
        if (option.value === "2") {
            radiosDiv.classList.remove("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = PUR;
            price = M;
            document.getElementById("radio1").checked = true;
        }
        if (option.value === "3") {
            checkboxDiv.classList.remove("d-none");
            radiosDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = L;
            checkbox.checked = false;
        }
    });
    radios.forEach(function (currentRadio) {
        currentRadio.addEventListener("change", function (event) {
            let radio = event.target;
            if (radio.value === "pur") {
                extraPrice = PUR;
            }
            if (radio.value === "red") {
                extraPrice = RED;
            }
            if (radio.value === "blue") {
                extraPrice = BLUE;
            }
        });
    });
    checkbox.addEventListener("change", function () {
        if (checkbox.checked) {
            extraPrice = CHB;
        } else {
            extraPrice = EXTRA_PRICE;
        }
    });
    calc.addEventListener("change", function () {
        if (myNum.value < 1) {
            myNum.value = 1;
        }
        result = price * myNum.value + extraPrice * myNum.value;
        resultSpan.innerHTML = result;
    });
});
